package spio.mszczepanowski.learningtogo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class Listview1 extends AppCompatActivity {

    int listaOd;
    int listaDo;
    ArrayList<Listview1Content> arrayList;
    ArrayList<Listview1Content> arrayListFiltr=new  ArrayList<>();

    Listview1Adapter adapter;

    Spinner spinnerLevelSelect;
    Spinner spinnerTimeSelect;

    ListView listview;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Wstawka balbiego
        Intent reciveIntent =getIntent();
        listaOd = reciveIntent.getIntExtra("listaOd",0);
        listaDo = reciveIntent.getIntExtra("listaDo",0);
        spinnerLevelSelect = (Spinner)findViewById(R.id.spinnerLevelSelect);
        spinnerTimeSelect  = (Spinner)findViewById(R.id.spinnerTimeSelect);



        arrayList = Listview1Content.getContent(listaOd,listaDo);
        adapter = new Listview1Adapter(this, arrayList);
        listview = (ListView) findViewById(R.id.listViewCourseContent);
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Intent myIntent;

                        myIntent = new Intent(v.getContext(), TaskUzupelnienie.class);
                        myIntent.putExtra("idKursu",arrayList.get(position).IdKursu);
                        startActivity(myIntent);

            }
        });


        Spinner spinnerLevel = (Spinner) findViewById(R.id.spinnerLevelSelect);
        ArrayAdapter<CharSequence> adapterSpinnerLevel = ArrayAdapter.createFromResource(this,
                R.array.spinner_levels, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(adapterSpinnerLevel);

        Spinner spinnerTime = (Spinner) findViewById(R.id.spinnerTimeSelect);
        ArrayAdapter<CharSequence> adapterSpinnerTime = ArrayAdapter.createFromResource(this,
                R.array.spinner_times, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTime.setAdapter(adapterSpinnerTime);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabListView);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Kliknij w interesujący Ciebie kurs, możesz użyć menu aby zawęzić liczbę kursów", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        spinnerLevelSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                przeladuj();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerTimeSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                przeladuj();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void przeladuj(){

        String spinerLeve = spinnerLevelSelect.getSelectedItem().toString();
        String spinerTime = spinnerTimeSelect.getSelectedItem().toString();

//        if (spinerLeve.equals("OFF")&& spinerTime.equals("OFF") ) {
//            Toast.makeText(Listview1.this, "Mam dwa ofy więc nie muszę mysleć", Toast.LENGTH_SHORT).show();//Wykomentować to potem jak nie bedzie potrzebne
//            arrayList = Listview1Content.getContent(listaOd,listaDo);
//            adapter = new Listview1Adapter(this, arrayList);
//            listview.setAdapter(adapter);
//
//        }else{

            int czastemp=0;
            arrayList.clear();
            arrayListFiltr = Listview1Content.getContent(listaOd,listaDo);

            if(!spinerTime.equals("Wybierz czas")) {
                czastemp = Integer.parseInt(spinerTime);
            }

            for(int i=0;i<arrayListFiltr.size();i++) {
                if ((spinerLeve.equals("Wybierz poziom") || spinerLeve.equals(arrayListFiltr.get(i).CourseLevel))&&(spinerTime.equals("Wybierz czas")||arrayListFiltr.get(i).CourseTime <= czastemp)){
                    arrayList.add(new Listview1Content(arrayListFiltr.get(i).CourseLevel,arrayListFiltr.get(i).CourseName,arrayListFiltr.get(i).CourseTime,arrayListFiltr.get(i).IdKursu));
                }
            }





            adapter = new Listview1Adapter(this, arrayList);
            listview.setAdapter(adapter);
//        }



    }






//
//        @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        Listview1 listview1 = Listview1Adapter.getItem(position);
//
////Here we use the Filtering Feature which we implemented in our Adapter class.
//        Listview1Adapter.getFilter().filter(listview1.toString(),new Filter.FilterListener() {
//            @Override
//            public void onFilterComplete(int count) {
//
//            }
//        });
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }




}

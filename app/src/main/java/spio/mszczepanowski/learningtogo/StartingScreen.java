package spio.mszczepanowski.learningtogo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class StartingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_screen);

        GridView gridviewSubjects = (GridView) findViewById(R.id.gridview_subjects);
        gridviewSubjects.setAdapter(new gridviewSubjectsAdapter(this));



        gridviewSubjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent myIntent;

                switch (position) {

                    case 0: //cooking
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",0);
                        myIntent.putExtra("listaDo",4);
                        startActivity(myIntent);
                        break;
                    case 1: //plane
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",5);
                        myIntent.putExtra("listaDo",9);
                        startActivity(myIntent);
                        break;
                    case 2: //sport
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",10);
                        myIntent.putExtra("listaDo",13);
                        startActivity(myIntent);
                        break;

                    case 3: //art
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",14);
                        myIntent.putExtra("listaDo",14);
                        startActivity(myIntent);
                        break;

                    case 4: //nature
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",15);
                        myIntent.putExtra("listaDo",15);
                        startActivity(myIntent);
                        break;
                    case 5: //animals
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",16);
                        myIntent.putExtra("listaDo",16);
                        startActivity(myIntent);
                        break;
                    case 6: //social
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",17);
                        myIntent.putExtra("listaDo",17);
                        startActivity(myIntent);
                        break;
                    case 7: //people
                        myIntent = new Intent(v.getContext(), Listview1.class);
                        myIntent.putExtra("listaOd",18);
                        myIntent.putExtra("listaDo",18);
                        startActivity(myIntent);
                        break;

                }

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabStartingScreen);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Kliknij w rysunek, aby przejść do wyboru kursu związanego z danym tematem", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toast.makeText(StartingScreen.this, "Jeśli nie wiesz co robić, kliknij w znak zapytania u góry ekranu", Toast.LENGTH_LONG).show();
    }

}

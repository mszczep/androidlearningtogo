package spio.mszczepanowski.learningtogo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WinActivity extends AppCompatActivity {


    int winWynik;
//    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);
        Intent intent = getIntent();
        winWynik=intent.getIntExtra("wynik",0);

        TextView textViewWin = (TextView) findViewById(R.id.textViewWinWynik);
        textViewWin.setText("Twój Wynik to: "+winWynik+"%");
        Button buttonYouWin = (Button) findViewById(R.id.buttonYouWin);


//        mediaPlayer = MediaPlayer.create(this, R.raw.wygrana);
//        mediaPlayer.start();

        buttonYouWin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                mediaPlayer.stop();
                finish();
            }
        });
    }
}

package spio.mszczepanowski.learningtogo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;


public class Listview1Adapter extends ArrayAdapter<Listview1Content> implements Filterable {
    public Listview1Adapter(Context context, ArrayList<Listview1Content> users) {
        super(context, 0, users);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Listview1Content lv1c = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.singleitem_listview1, parent, false);
        }

        TextView singleitem_Level = (TextView) convertView.findViewById(R.id.singleitem_textViewLevel);
        TextView singleitem_Name = (TextView) convertView.findViewById(R.id.singleitem_textViewName);
        TextView singleitem_Time = (TextView) convertView.findViewById(R.id.singleitem_textViewTime);

        singleitem_Level.setText(lv1c.CourseLevel);
        singleitem_Name.setText(lv1c.CourseName);
        singleitem_Time.setText("" + lv1c.CourseTime);
        return convertView;
    }

    }

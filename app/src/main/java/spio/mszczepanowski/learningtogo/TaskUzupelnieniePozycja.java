package spio.mszczepanowski.learningtogo;

/**
 * Created by Android on 2017-06-04.
 */

public class TaskUzupelnieniePozycja {

    int prawidolowyWybor;
    String content1;
    String wybur1;
    String wybur2;
    String wybur3;
    String wybur4;

    public int getPrawidolowyWybor() {
        return prawidolowyWybor;
    }

    public void setPrawidolowyWybor(int prawidolowyWybor) {
        this.prawidolowyWybor = prawidolowyWybor;
    }

    public TaskUzupelnieniePozycja(int prawidolowyWybor, String content1, String wybur1, String wybur2, String wybur3, String wybur4) {
        this.prawidolowyWybor = prawidolowyWybor;
        this.content1 = content1;
        this.wybur1 = wybur1;
        this.wybur2 = wybur2;
        this.wybur3 = wybur3;
        this.wybur4 = wybur4;
    }


    public String getContent() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getWybur1() {
        return wybur1;
    }

    public void setWybur1(String wybur1) {
        this.wybur1 = wybur1;
    }

    public String getWybur2() {
        return wybur2;
    }

    public void setWybur2(String wybur2) {
        this.wybur2 = wybur2;
    }

    public String getWybur3() {
        return wybur3;
    }

    public void setWybur3(String wybur3) {
        this.wybur3 = wybur3;
    }

    public String getWybur4() {
        return wybur4;
    }

    public void setWybur4(String wybur4) {
        this.wybur4 = wybur4;
    }
}

package spio.mszczepanowski.learningtogo;

import java.util.ArrayList;

public class Listview1Content {


    public String CourseLevel;
    public String CourseName;
    public int CourseTime;
    public int IdKursu;

    public Listview1Content(String CourseLevel, String CourseName, int CourseTime,int IdKursu)
    {
        this.CourseLevel = CourseLevel;
        this.CourseName = CourseName;
        this.CourseTime = CourseTime;
        this.IdKursu = IdKursu;
    }

    public static ArrayList<Listview1Content> getContent(int listaOd,int listaDo) {

        ArrayList<Listview1Content> addContent = new ArrayList<>();

        TaskUzupelnienieList taskUzupelnienieList;
        String poziom;
        String nazwa;
        int czas;
        for(int i=listaOd; i<=listaDo; i++) {
            taskUzupelnienieList = new TaskUzupelnienieList(i);

            poziom=taskUzupelnienieList.getPoziom();
            nazwa= taskUzupelnienieList.getNazwaKursu();
            czas=taskUzupelnienieList.getCzasKursu();

            addContent.add(new Listview1Content(poziom,nazwa,czas,i));

        }

        return addContent;
    }
}

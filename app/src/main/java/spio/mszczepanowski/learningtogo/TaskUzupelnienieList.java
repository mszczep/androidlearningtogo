package spio.mszczepanowski.learningtogo;

import java.util.ArrayList;


/**
 * Created by Android on 2017-06-04.
 */

public class TaskUzupelnienieList {

    ArrayList<TaskUzupelnieniePozycja> taskuzupelnienielista = new ArrayList<>();

    String nazwaKursu;
    String poziom;
    int czasKursu;


    public String getNazwaKursu() {
        return nazwaKursu;
    }

    public String getPoziom() {
        return poziom;
    }

    public int getCzasKursu() {
        return czasKursu;
    }

    public TaskUzupelnieniePozycja get(int pozycja) {

        return taskuzupelnienielista.get(pozycja);
    }

    public void set(TaskUzupelnieniePozycja task, int pozycja) {

        this.taskuzupelnienielista.set(pozycja, task);
    }

    public void add(TaskUzupelnieniePozycja task) {

        this.taskuzupelnienielista.add(task);
    }

    public int lengh()  //Zrobić to cholerstwo tak aby działało;
    {
        return this.taskuzupelnienielista.size();
    }

    public TaskUzupelnienieList(int IdKursu) {

        TaskUzupelnieniePozycja item;

        switch (IdKursu) {

            case 0://///////////////////KURS DLA ID 0///////////////////////////////////////////////////////////////////////////////////////////////////////////
                nazwaKursu ="Ingredients";
                poziom ="A1";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (2, "A round, red vegetable you will need to cook spaghetti is called a ______.", "potato", "tomato", "cucumber", "cabbage");
                this.add(item);

                item = new TaskUzupelnieniePozycja
                        (1, "_______ with maple syrup is a popular breakfast in the U.S.", "Pancakes", "crepes", "soup", "coffee");
                this.add(item);

                item = new TaskUzupelnieniePozycja
                        (2, "I usually eat _______ with milk for breakfast.", "bacon", "cereal", "butter", "salt");
                this.add(item);

                item = new TaskUzupelnieniePozycja
                        (4, "Nina left milk and ______ for Santa. ", "tomatoes", "caramel apple", "bread", "cookies");
                this.add(item);
                break;
            case 1:///////////////////KURS DLA ID 1///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                nazwaKursu ="Cooking equipment";
                poziom ="A1";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (1, "Which kitchen equipment do you need to make scrambled eggs?", "a frying pan", "a rolling pin", "a juicer", "a freezer");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "Which kitchen equipment do you need to make sponge cake?", "a spatula", "a baking pan", "a toaster", "a frying pan");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                    (3, "Which kitchen equipment do you need to make ice cream?", "a cookie cutter", "an oven mitt", "a freezer", "an oven");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                    (1, "Which kitchen equipment do you need to make pasta?", "a pot", "an ice tray", "a tin opener", "a fridge");
                this.add(item);
                break;
            case 2:///////////////////KURS DLA ID 2///////////////////////////////////////////
                nazwaKursu ="Recipes";
                poziom ="A2";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (2, "___ the carrot into small circles.", "Drain", "Chop", "Stir", "Squeeze");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "___ the lasagne for 30 minutes in the oven.", "Bake", "Mix", "Boil", "Burn");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "After ten minutes, ___ the pasta until there is no water left. Then place the pasta into a large bowl.", "fry", "boil", "drain", "bake");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "___ the onion and throw away the skin.", "Stir", "Peel", "Fry", "Boil");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "___ the steak with salt, pepper and lemon.", "Chop", "Drain", "Season", "Stir");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "___ the onion until it is soft, but not brown.", "Fry", "Pour", "Bake", "Mix");
                this.add(item);
                break;
            case 3:///////////////////KURS DLA ID 3///////////////////////////////////////////
                nazwaKursu ="Cooking techniques 1";
                poziom ="B1";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (1, "To cook in an oven by using heat.", "bake", "boil", "stir", "fry");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "To coat with oil or butter.", "melt", "grease", "mince", "chop");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "To transfer liquid from one container to another.", "pour", "put", "mix", "chop");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "To cook by putting the food into extremely hot oil.", "roast", "steam", "fry", "bake");
                this.add(item);
                break;
            case 4:///////////////////KURS DLA ID 4///////////////////////////////////////////
                nazwaKursu ="Cooking techniques 2";
                poziom ="B2";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (1, "To bake is to cook in ____.", "an oven", "a wok", "a pan", "a pot");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "To grease is to coat with _____.", "water", "butter", "flour", "cheese");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "You can pour____. ", "liquids", "flour", "butter", "carrots");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "To fry is to cook in extremely hot ____.", "water", "oven", "oil", "fridge");
                this.add(item);
                break;
            case 5:///////////////////KURS DLA ID 5///////////////////////////////////////////
                nazwaKursu ="Travelling by plane";
                poziom ="A1";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (3, "Passenger A: _____ me, I’m afraid you’re sitting in the wrong _____.", "Sorry / aisle", "Excuse / aisle", "Excuse / seat", "Sorry / seat");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "Passenger B: Really? Let me _____. My seat is 26 A.", "check", "apologies", "sorry", "go");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "Passenger A: I suppose 26A is a _____ seat, not an aisle seat. So yours is next to this seat.", "plane", "window", "toilet", "train");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "Passenger B: I’m ____, I misunderstood.", "incorrect", "innocent", "sorry", "apologies");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "No ____.", "welcome", "problem", "understand", "fine");
                this.add(item);
                break;
            case 6:///////////////////KURS DLA ID 6///////////////////////////////////////////
                nazwaKursu ="Travel - basic vocabulary";
                poziom ="A1";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (1, "You stay there when you are on holidays.", "a hotel", "a plane", "a boat", "the sea");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "A proof that you paid for the travel.", "a paper", "vacation", "a ticket", "a train");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "You usually fly by ____.", "a car", "a plane", "a ship", "a train");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "A sandy place by the sea.", "a lake", "a forest", "a beach", "a street");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A person who walks you around a city.", "a police officer", "a tourist guide", "a flight attendant", "a pilot");
                this.add(item);
                break;
            case 7:///////////////////KURS DLA ID 7///////////////////////////////////////////
                nazwaKursu ="Asking for directions";
                poziom ="A2";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (2, "Excuse me, could you help me, please?", "Yes. What are the directions?", "Yes. What is it?", "I did.", "Yes. Yesterday.");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "Can you tell me where I can find a bank?", "The department store is the place to go.", "Is close to Woodpeckerʼs department store.", "It will be there in a minute", "I went there yesterday.");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "Could you please tell me the directions to the bank?", "You go down this street then turn left…", "Absolutely. Hm… the best spot to visit in this town is…", "I had no idea!", "Why wouldn’t you?");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "Itʼs a bit confusing. Can you show it on the map?", "You are here right now the bank is right here…", "The bank may not have any maps", "The is one on the left.", "There was one there…");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "Thank you very much!", "You’re welcomed!", "You’re welcome!", "Welcomed!", "Tomorrow.");
                this.add(item);
                break;
            case 8:///////////////////KURS DLA ID 8///////////////////////////////////////////
                nazwaKursu ="Traveling experience";
                poziom ="C1";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (3, "Going off and travelling alone can be a very _____ experience.", "curve", "fundamental", "liberating","wicked");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "Certain _____ are required of a lone traveller to enable him or her to get by safely and securely.", "qualities", "preparation", "curve", "liberating");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A good map is _____ and so is some knowledge of the public transport system.", "specific", "invaluable", "liberating", "qualities");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "How to go about it begins with your own self-awareness, followed by some thorough ______.", "qualities", "fundamentals", "preparation", "curve");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "It's a steep learning _____ that we attempt to scale when we set off on an adventure alone.", "summit", "curve", "experience", "qualities");
                this.add(item);
                break;
            case 9:///////////////////KURS DLA ID 9///////////////////////////////////////////
                nazwaKursu ="Places - advanced vocabulary";
                poziom ="C1";
                czasKursu =15;

                item = new TaskUzupelnieniePozycja
                        (1, "This is the city that a major airline has its headquarters at.", "gateway city", "capitol city", "tourist trap", "layover");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "This is a vacation put together by the travel agent. The price includes travel, lodging and food.", "package tour", "tourist trap", "working vacation", "holidays");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A restaurant where the prices are high to take advantage of the tourists who don’t know where else to go.", "gateway city", "tourist trap", "layover", "pub");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "Places that are less known to tourists are described as being ______.", "packages tours", "tourist traps", "off the beaten path", "far away");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "This is a trip that someone may take for business that also involves pleasure.", "package tour", "working vacation", "tourist trap", "all inclusive");
                this.add(item);
                break;


            case 10:///////////////////KURS DLA ID 10///////////////////////////////////////////
                nazwaKursu ="Sports disciplines";
                poziom ="B1";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (1, "A team sport in which two teams of six players are separated by a net. Each team tries to score points by grounding a ball on the other team's court under organized rules.", "volleyball", "handball", "basketball", "football");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "A team sport in which two teams of seven players each (six outfield players and a goalkeeper) pass a ball using their hands with the aim of throwing it into the goal of the other team.", "volleyball", "football", "netball", "basketball");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A sport played between two teams of skaters on a large flat area of ice, using a three-inch-diameter (76.2 mm) vulcanized rubber disc called a puck.", "ice skating", "ice hockey", "bandy", "handball");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "A non-contact sport played on a rectangular court. While most often played as a team sport with five players on each side, three-on-three, two-on-two, and one-on-one competitions are also common.", "football", "volleyball", "basketball", "handball");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A bat-and-ball game played between two teams of eleven players on a field, at the centre of which is a rectangular 22-yard-long pitch with a wicket (a set of three wooden stumps) at each end. One team bats, attempting to score as many runs as possible, whilst their opponents field.", "baseball", "cricket", "tennis", "football");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                    (2, "A racket sport that can be played individually against a single opponent or between two teams of two players each. Each player uses a racket that is strung with cord to strike a hollow rubber ball covered with felt over or around a net and into the opponent's court.", "cricket", "tennis", "baseball", "hockey");
                this.add(item);
                break;
            case 11:///////////////////KURS DLA ID 11///////////////////////////////////////////
                nazwaKursu ="Buying tickets";
                poziom ="B2";
                czasKursu =5;

                item = new TaskUzupelnieniePozycja
                        (1, "I'd like four tickets please.", "Would you like front row seats?", "How much are the tickets?", "Who's winning?", "Who's playing today?");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "That sounds great, but how much do the tickets cost?", "They're £30 every.", "They're £30 any.", "They're £30 each.", "They're £30 for.");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "Oh that's fine. I'd like to reserve four front-row seats for this Saturday's match.", "Ok, that'll be tomorrow", "Ok, that'll be £120.", "Which half are we in?", "What's the score?");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "Here you go.", "I know. So, shall we swap them?", "Yes, they say it's fine. ", "Thank you. Here are your tickets.", "OK, we'll see you on Saturday.");
                this.add(item);
                break;
            case 12:///////////////////KURS DLA ID 12///////////////////////////////////////////
                nazwaKursu ="Sport phrasal verbs";
                poziom ="C1";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (2, "to warm up", "to surrender", "to do stretching to get your body ready for exercise", "to leave a competition or race", "to gain weight and muscle");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "to cool down", "to leave a competition or race", "to do stretching to get your body ready for exercise", "to do movements after your main exercise", "to gain weight and muscle");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "to join in", "to take part in a sporting event or match", "to leave a competition or race", "to do exercise routines/sessions", "to leave a competition or race");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "to give up", "to leave a competition or race", "to gain weight and muscle", "to surrender/quit", "to leave a competition or race");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "to knock someone out", "to leave a competition or race", "to hit someone in a boxing match so that they become unconscious", "to surrender/quit", "gain weight and muscle");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "to bulk up", "to leave a competition or race", "to surrender/quit", "gain weight and muscle", "to leave a competition or race");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (1, "to work out", "	To do exercise routines/sessions", "to surrender/quit", "to leave a competition or race", "to leave a competition or race");
                this.add(item);
                break;
            case 13:///////////////////KURS DLA ID 13///////////////////////////////////////////
                nazwaKursu ="Sport idioms";
                poziom ="C2";
                czasKursu =10;

                item = new TaskUzupelnieniePozycja
                        (2, "Another string to your bow", "To do what you are told to do", "Another skill/ability", "Without any problems", "To do what you are told to do");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "To dive in head first", "To start a relationship badly", "To do what you are told to do", "To start doing something 100%", "Without any problems");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "To get off on the wrong foot", "Without any problems", "To start a relationship badly", "To be far superior to someone else", "To do something wrong");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "To jump through the hoops", "To do something wrong", "To do something right ", "To do what you are told to do", "Without any problems");
                this.add(item);

                item = new TaskUzupelnieniePozycja
                        (1, "To do something off your own bat", "To take the initiative", "To do something right ", "To start doing something 100%", "To start a relationship badly");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "To get off to a flying start", "To start a relationship badly", "To have a successful start", "To do something wrong", "With great difficulty");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "To be on the ball", "To have a successful start", "To do something right", "To be sharp and to know everything that’s happening", "To take the initiative");
                this.add(item);
                break;

            case 14:
                nazwaKursu="Art - basic vocabulary";
                poziom="A1";
                czasKursu=5;

                item = new TaskUzupelnieniePozycja
                        (1, "An artist who signs is called...", "a singer", "a painter", "a spatula", "a night");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "The painter uses __ to paint.", "a spatula", "a microphone", "a brush", "a soap");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "The painter paints on...", "tomorrow", "a canvas", "the floor", "a day");
                this.add(item);

                break;

            case 15:
                nazwaKursu="Nature - basic vocabulary";
                poziom="A1";
                czasKursu=5;

                item = new TaskUzupelnieniePozycja
                        (1, "A place with many trees is called __.", "a forest", "a painter", "a congregation", "a getaway");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (3, "That is a very pretty __", "murder", "copier", "flower", "tomorrow");
                this.add(item);

                break;

            case 16:
                nazwaKursu="Animals - grouping vocabulary";
                poziom="B1";
                czasKursu=1;

                item = new TaskUzupelnieniePozycja
                        (3, "The wolves hunt in _.", "murders", "groups", "packs", "vans");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A group of ravens is called _.", "a pack", "a murder", "a congregation", "a day");
                this.add(item);

                break;


            case 17:
                nazwaKursu="Test";
                poziom="B1";
                czasKursu=1;

                item = new TaskUzupelnieniePozycja
                        (4, "A person who often mistakes peoples names is called _.", "Lukasz", "Natalia", "Kasia", "Maciej");
                this.add(item);

                break;

            case 18:
                nazwaKursu="Jobs";
                poziom="B2";
                czasKursu=5;

                item = new TaskUzupelnieniePozycja
                        (3, "A person who assists a doctor is called a __", "physician", "constructor", "nurse", "therapist");
                this.add(item);
                item = new TaskUzupelnieniePozycja
                        (2, "A person who catches criminals is NOT called a __.", "police officer", "civil officer", "whistleblower", "bobby");
                this.add(item);

                break;
        }
    }
}


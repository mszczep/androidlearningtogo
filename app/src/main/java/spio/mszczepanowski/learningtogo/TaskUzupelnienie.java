package spio.mszczepanowski.learningtogo;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.button;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;
import static android.os.Build.VERSION_CODES.N;

public class TaskUzupelnienie extends AppCompatActivity {


    //deklaracja przycisków;
    protected Button buttonTUwybor1;
    protected Button buttonTUwybor2;
    protected Button buttonTUwybor3;
    protected Button buttonTUwybor4;
    protected Button buttonTUnext;

    protected String buttonTUwybor1State = "start";
    protected String buttonTUwybor2State = "start";
    protected String buttonTUwybor3State = "start";
    protected String buttonTUwybor4State = "start";
    protected String buttonTUnextState = "off";


    //deklaracja pól tekstowych;
    TextView textViewTUZdanie;
    TextView textViewTUOpis;
    TextView textViewStaty;

    //Dane globalne
    protected int prawidlowyWybor;
    protected int pozycjaKursu;
    protected int IdKursu;    ////WAZNE STWORZYĆ COŚ MĄDRZEJSZEGO;
    protected int dlugoscKursu;

    protected float odpOK=0;
    protected float odpNOT=0;
    protected int wynik=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_uzupelniene);

        JoinActivityObjects ();
        //ładowanie ustawień po obrocie;
        if(savedInstanceState==null){
            pozycjaKursu=0;
            Intent reciveIntent =getIntent();
            IdKursu = reciveIntent.getIntExtra("idKursu",0);

        }else{
            pozycjaKursu=savedInstanceState.getInt("pozycjaKursu",0);
            pozycjaKursu=savedInstanceState.getInt("IdKursu",0);

            buttonTUwybor1State=savedInstanceState.getString("buttonTUwybor1State");
            buttonTUwybor2State=savedInstanceState.getString("buttonTUwybor2State");
            buttonTUwybor3State=savedInstanceState.getString("buttonTUwybor3State");
            buttonTUwybor4State=savedInstanceState.getString("buttonTUwybor4State");
            buttonTUnextState=savedInstanceState.getString("buttonTUnextState");

        }

        //To niżej jest brzydkie potem to się dopracuje (nie ma jak to dług technologiczny);
        buttonTUwybor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TUWeryfikuj(prawidlowyWybor,1)){
                    buttonTUwybor1State="downOK";
                    buttonTUwybor2State="downBAD";
                    buttonTUwybor3State="downBAD";
                    buttonTUwybor4State="downBAD";

                    buttonTUnextState="on";

                    odpOK++;
                }else{
                    buttonTUwybor1State="downBAD";
                    odpNOT++;
                }

                loadDate(IdKursu,pozycjaKursu);
            }
        });

        buttonTUwybor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TUWeryfikuj(prawidlowyWybor,2)){
                    buttonTUwybor1State="downBAD";
                    buttonTUwybor2State="downOK";
                    buttonTUwybor3State="downBAD";
                    buttonTUwybor4State="downBAD";

                    buttonTUnextState="on";

                    odpOK++;
                }else{
                    buttonTUwybor2State="downBAD";
                    odpNOT++;
                }

                loadDate(IdKursu,pozycjaKursu);

            }
        });

        buttonTUwybor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TUWeryfikuj(prawidlowyWybor, 3)) {
                    buttonTUwybor1State="downBAD";
                    buttonTUwybor2State="downBAD";
                    buttonTUwybor3State="downOK";
                    buttonTUwybor4State="downBAD";

                    buttonTUnextState="on";
                    odpOK++;
                } else {
                    buttonTUwybor3State="downBAD";
                    odpNOT++;
                }

                loadDate(IdKursu,pozycjaKursu);
            }
        });

        buttonTUwybor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TUWeryfikuj(prawidlowyWybor, 4)) {
                    buttonTUwybor1State="downBAD";
                    buttonTUwybor2State="downBAD";
                    buttonTUwybor3State="downBAD";
                    buttonTUwybor4State="downOK";

                    buttonTUnextState="on";
                    odpOK++;

                } else {
                    buttonTUwybor4State="downBAD";
                    odpNOT++;
                }

                loadDate(IdKursu,pozycjaKursu);
            }
        });

        buttonTUnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pozycjaKursu = pozycjaKursu+1;

                if(pozycjaKursu ==dlugoscKursu){

                    pozycjaKursu=dlugoscKursu-1;
                    Intent youWin = new Intent(v.getContext(), WinActivity.class);
                    youWin.putExtra("wynik",wynik);

                    startActivity(youWin);
                    finish();
//                    Toast.makeText(TaskUzupelnienie.this, "Wezwać lekarza zakres poza tablicą ", Toast.LENGTH_SHORT).show();

                }

                buttonTUwybor1State="start";
                buttonTUwybor2State="start";
                buttonTUwybor3State="start";
                buttonTUwybor4State="start";

                buttonTUnextState="off";

                loadDate(IdKursu,pozycjaKursu);
            }
        });

        loadDate(IdKursu,pozycjaKursu); //Jak dojdzie w silniku obsłóga wielu kursów to nie będzie już PituPitu to jest tylk MOOK(tak koło 12-14 sprintu);


    }

    //Sprawdza poprawność odpowiedzi
    protected boolean TUWeryfikuj(int prawidlowaPozycja,int NrButtona){
        if( prawidlowaPozycja ==NrButtona){
//            Toast.makeText(TaskUzupelnienie.this, "Dobra Odpowiedz", Toast.LENGTH_SHORT).show();
            buttonTUnext.setVisibility(View.VISIBLE);
            return true;

        }else{
//            Toast.makeText(TaskUzupelnienie.this, "Zła Odpowiedz", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    //łączy obiekty z klasy z obiektami na aktywnosci; //metoda w budowie;
    protected void JoinActivityObjects (){

        textViewTUOpis   =(TextView) findViewById(R.id.textViewTUOpis);
        textViewTUZdanie =(TextView) findViewById(R.id.textViewTUZdanie);
        textViewStaty    =(TextView) findViewById(R.id.textViewStaty);


        buttonTUwybor1   = (Button) findViewById(R.id.buttonTUwybor1);
        buttonTUwybor2   = (Button) findViewById(R.id.buttonTUwybor2);
        buttonTUwybor3   = (Button) findViewById(R.id.buttonTUwybor3);
        buttonTUwybor4   = (Button) findViewById(R.id.buttonTUwybor4);

        buttonTUnext     = (Button) findViewById(R.id.buttonTUnext);



    }

    //ładuje content i dane globalne do aktywności;  //metoda w budowie;
    protected void loadDate(int idKursu, Integer pozycjaKursu){

        TaskUzupelnienieList taskUzupelnienieList =new TaskUzupelnienieList(idKursu);  //to jest tu na potrzeby dużej reflektoryzacji kodu potem zniknie;

        //ładowanie do obiektów View;
        if((odpOK+odpNOT)==0){               //(odpOK+odpNOT)==0
            wynik=0;

        }else{
            wynik = Math.round((odpOK/(odpOK+odpNOT))*100);

         }




        textViewStaty.setText("Page:"+(pozycjaKursu+1)+"/"+taskUzupelnienieList.lengh()+"   Score: "+wynik+"%");//////////////////////////////////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!






        textViewTUOpis.setText("Choose the correct answer.");
        textViewTUZdanie.setText(taskUzupelnienieList.get(pozycjaKursu).getContent());

        buttonTUwybor1.setText(taskUzupelnienieList.get(pozycjaKursu).getWybur1());
        buttonTUwybor2.setText(taskUzupelnienieList.get(pozycjaKursu).getWybur2());
        buttonTUwybor3.setText(taskUzupelnienieList.get(pozycjaKursu).getWybur3());
        buttonTUwybor4.setText(taskUzupelnienieList.get(pozycjaKursu).getWybur4());

        if(buttonTUwybor1State=="start"){
            buttonTUwybor1.setTextColor(Color.BLACK);
            buttonTUwybor1.setClickable(true);
        }
        if(buttonTUwybor1State=="downOK"){
            buttonTUwybor1.setTextColor(Color.parseColor("#019624"));
            buttonTUwybor1.setClickable(false);
        }
        if(buttonTUwybor1State=="downBAD"){
            buttonTUwybor1.setTextColor(Color.RED);
            buttonTUwybor1.setClickable(false);
        }

        if(buttonTUwybor2State=="start"){
            buttonTUwybor2.setTextColor(Color.BLACK);
            buttonTUwybor2.setClickable(true);
        }
        if(buttonTUwybor2State=="downOK"){
            buttonTUwybor2.setTextColor(Color.parseColor("#019624"));
            buttonTUwybor2.setClickable(false);
        }
        if(buttonTUwybor2State=="downBAD"){
            buttonTUwybor2.setTextColor(Color.RED);
            buttonTUwybor2.setClickable(false);
        }

        if(buttonTUwybor3State=="start"){
            buttonTUwybor3.setTextColor(Color.BLACK);
            buttonTUwybor3.setClickable(true);
        }
        if(buttonTUwybor3State=="downOK"){
            buttonTUwybor3.setTextColor(Color.parseColor("#019624"));
            buttonTUwybor3.setClickable(false);
        }
        if(buttonTUwybor3State=="downBAD"){
            buttonTUwybor3.setTextColor(Color.RED);
            buttonTUwybor3.setClickable(false);
        }

        if(buttonTUwybor4State=="start"){
            buttonTUwybor4.setTextColor(Color.BLACK);
            buttonTUwybor4.setClickable(true);
        }
        if(buttonTUwybor4State=="downOK"){
            buttonTUwybor4.setTextColor(Color.parseColor("#019624"));
            buttonTUwybor4.setClickable(false);
        }
        if(buttonTUwybor4State=="downBAD"){
            buttonTUwybor4.setTextColor(Color.RED);
            buttonTUwybor4.setClickable(false);
        }

        if(buttonTUnextState=="off"){
            buttonTUnext.setVisibility(View.GONE);
        }
        if(buttonTUnextState=="on"){
            buttonTUnext.setVisibility(View.VISIBLE);
        }




        //ładowanie do zmiennych
        prawidlowyWybor = taskUzupelnienieList.get(pozycjaKursu).getPrawidolowyWybor();
        dlugoscKursu    = taskUzupelnienieList.lengh();


    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt("pozycjaKursu",pozycjaKursu);

        savedInstanceState.putString("buttonTUwybor1State",buttonTUwybor1State);
        savedInstanceState.putString("buttonTUwybor2State",buttonTUwybor2State);
        savedInstanceState.putString("buttonTUwybor3State",buttonTUwybor3State);
        savedInstanceState.putString("buttonTUwybor4State",buttonTUwybor4State);
        savedInstanceState.putString("buttonTUnextState",buttonTUnextState);
        savedInstanceState.putInt("IdKursu",IdKursu);




    }
}

# Learning To Go  

This is a learning app that is focused on teaching english to polish persons through a set of short courses.
The app was written as an agile development exercise, during the course of my software development studies. 
We were given only 4 days to finish the project, so it is not really tested on many devices.

Starting screen

![Screenshot 1](img/ss1.png)

Course Selection

![Screenshot 2](img/ss2.png)


![Screenshot 3](img/ss3.png)

Course content

![Screenshot 4](img/ss4.png)


Course finished screen

![Screenshot 5](img/ss5.png)